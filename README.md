#Get all artist and lyrics of songs at http://j-lyric.net

##1. Runnable file JAR
    
    cd package
    ### Read default options ###
    java -jar app.jar
    ### Pass options from properties file ###
    java -Dconfig=application.properties -jar app.jar

##2. Database

See file: `db\lyricstore.sql`