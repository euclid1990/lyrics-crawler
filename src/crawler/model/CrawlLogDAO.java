package crawler.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import crawler.object.CrawlLog;

public class CrawlLogDAO {
	private DAOManager daoMng;
	private String tableName = "crawl_logs";

	public CrawlLogDAO()
	{
		try {
			this.daoMng = DAOManager.getInstance();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	}

	public int insert(CrawlLog crawlLog)
	{
		int id = 0;
		try {
			String query = "INSERT INTO lyricstore." + tableName + " (page, url, created_at, updated_at) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStmt = this.daoMng.conn.prepareStatement(
            	query, Statement.RETURN_GENERATED_KEYS
            );
            preparedStmt.setInt(1, crawlLog.getPage());
            preparedStmt.setString(2, crawlLog.getUrl());
            preparedStmt.setTimestamp(3, crawlLog.getCurrentTimeStamp());
            preparedStmt.setTimestamp(4, crawlLog.getCurrentTimeStamp());
            preparedStmt.executeUpdate();
            ResultSet rs = preparedStmt.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            preparedStmt.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            return id;
        }
	}
}
