package crawler.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import crawler.object.Artist;

public class ArtistDAO {
	private DAOManager daoMng;
	private String tableName = "artists";

	public ArtistDAO()
	{
		try {
			this.daoMng = DAOManager.getInstance();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	}

	public int insert(Artist artist)
	{
		int id = isExisting(artist);
		if (id != 0) {
			return id;
		}
		try {
			String query = "INSERT INTO lyricstore." + tableName + " (artist_id_crawler, name, created_at, updated_at) VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStmt = this.daoMng.conn.prepareStatement(
            	query, Statement.RETURN_GENERATED_KEYS
            );
            preparedStmt.setString(1, artist.getArtistIdCrawler());
            preparedStmt.setString(2, artist.getName());
            preparedStmt.setTimestamp(3, artist.getCurrentTimeStamp());
            preparedStmt.setTimestamp(4, artist.getCurrentTimeStamp());
            preparedStmt.executeUpdate();
            ResultSet rs = preparedStmt.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            preparedStmt.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            return id;
        }
	}

	public int isExisting(Artist artist)
	{
		int id = 0;
		try {
			String query = "SELECT * from lyricstore." + tableName + " WHERE artist_id_crawler = ?";
            PreparedStatement preparedStmt = this.daoMng.conn.prepareStatement(
            	query
            );
            preparedStmt.setString(1, artist.getArtistIdCrawler());
            ResultSet rs = preparedStmt.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            preparedStmt.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            return id;
        }
	}
}
