package crawler.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DAOManager {
    // JDBC driver name and database URL
    public static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static String DB_URL = "jdbc:mysql://localhost/lyricstore?characterEncoding=utf8";

    //  Database credentials
    public static String DB_USER = "root";
    public static String DB_PASS = "123456789";

    // Database connection
    Connection conn = null;
    Statement stmt = null;

    /**
     * Implementation for one connection per user by singleton class
     *
     */
    private static class DAOManagerSingleton {
    	// Use ThreadLocal for one instance per thread.
        public static ThreadLocal<DAOManager> INSTANCE;
        static
        {
        	ThreadLocal<DAOManager> dm;
            try {
                dm = new ThreadLocal<DAOManager>() {
                	@Override protected DAOManager initialValue() {
                		try {
                            return new DAOManager();
                        } catch (Exception e) {
                        	e.printStackTrace();
                            return null;
                        }
                	}
                };
            } catch (Exception e) {
            	e.printStackTrace();
            	dm = null;
            }
            INSTANCE = dm;
        }
    }

    /**
     * A private Constructor prevents any other class from instantiating.
     *
     */
    public static DAOManager getInstance() {
        return DAOManagerSingleton.INSTANCE.get();
    }

    public DAOManager() throws Exception
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Created DB Connection....");
        } catch (ClassNotFoundException e) {
            // Handle errors for Class.forName
            throw e;
        } catch (Exception e) {
            // Handle errors for Exception
            throw e;
        }
    }

    public void open() throws SQLException {
        try {
            if (this.conn == null && this.stmt == null) {
            	this.conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            	this.stmt = conn.createStatement();
            }
        } catch (SQLException e) {
            // Handle errors for JDBC
        	e.printStackTrace();
            throw e;
        }
    }

    public void close() throws SQLException
    {
        try {
            if (this.conn != null)
                this.conn.close();
        } catch(SQLException e) {
        	// Handle errors for JDBC
        	e.printStackTrace();
        	throw e;
        }
    }

}