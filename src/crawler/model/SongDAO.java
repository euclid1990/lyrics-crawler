package crawler.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import crawler.object.Song;

public class SongDAO {
	private DAOManager daoMng;
	private String tableName = "songs";

	public SongDAO()
	{
		try {
			this.daoMng = DAOManager.getInstance();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
	}

	public int insert(Song song)
	{
		int id = isExisting(song);
		if (id != 0) {
			return id;
		}
		try {
			String query = "INSERT INTO lyricstore." + tableName + " (song_id_crawler, artist_id, name, lyrictist, composer, lyrics, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = this.daoMng.conn.prepareStatement(
            	query, Statement.RETURN_GENERATED_KEYS
            );
            preparedStmt.setString(1, song.getSongIdCrawler());
            preparedStmt.setInt(2, song.getArtistId());
            preparedStmt.setString(3, song.getName());
            preparedStmt.setString(4, song.getLyrictist());
            preparedStmt.setString(5, song.getComposer());
            preparedStmt.setString(6, song.getLyrics());
            preparedStmt.setTimestamp(7, song.getCurrentTimeStamp());
            preparedStmt.setTimestamp(8, song.getCurrentTimeStamp());
            preparedStmt.executeUpdate();
            ResultSet rs = preparedStmt.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            preparedStmt.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            return id;
        }
	}

	public int isExisting(Song song)
	{
		int id = 0;
		try {
			String query = "SELECT * from lyricstore." + tableName + " WHERE song_id_crawler = ?";
            PreparedStatement preparedStmt = this.daoMng.conn.prepareStatement(
            	query
            );
            preparedStmt.setString(1, song.getSongIdCrawler());
            ResultSet rs = preparedStmt.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            preparedStmt.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
            return id;
        }
	}
}
