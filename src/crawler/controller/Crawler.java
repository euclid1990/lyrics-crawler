package crawler.controller;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import crawler.model.ArtistDAO;
import crawler.model.CrawlLogDAO;
import crawler.model.SongDAO;
import crawler.object.Artist;
import crawler.object.CrawlLog;
import crawler.object.Song;

public class Crawler {
	public static String urlMain = "http://j-lyric.net";
	public static String urlArtist = "http://j-lyric.net/artist/p%d.html";
	public static int START_PAGE = 1;
	public static int END_PAGE = 1;
	public static int TIME_OUT = 5000;

	public static ArtistDAO artistModel = new ArtistDAO();
	public static SongDAO songModel = new SongDAO();
	public static CrawlLogDAO logModel = new CrawlLogDAO();

	public Crawler()
	{
	}

	public void fetch()
	{
		for (int i=START_PAGE; i<=END_PAGE; i++) {
			String url = String.format(urlArtist, i);
			try {
				CrawlLog crawlLog = new CrawlLog();
				crawlLog.setPage(i);
				crawlLog.setUrl(url);
				logModel.insert(crawlLog);
				this.fetchArtist(url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void fetchArtist(String url) throws IOException
	{
		Document doc = Jsoup.connect(url)
							.userAgent("Mozilla")
							.timeout(TIME_OUT)
							.get();
		Elements listArtist = doc.select("#sideArtist > .body");
		for (Element artistElm : listArtist) {
			Element artistSelector = artistElm.select("a[href].artist").first();
			String artistHref = urlMain + artistSelector.attr("href");
			String artistName = artistSelector.text();
			String artistIdCrawler = artistSelector.attr("href").replaceAll("/artist/([a-zA-Z0-9-_]*)/", "$1");
			Artist artist = new Artist();
			artist.setArtistIdCrawler(artistIdCrawler);
			artist.setName(artistName);
			int artistId = artistModel.insert(artist);
			System.out.println("## " + artistName + " > " + artistHref + " > " + artistId);
			this.fetchSong(artistHref, artistId);

		}
	}

	public void fetchSong(String url, int artistId) throws IOException
	{
		System.out.println("=============== START ===============");
		Document doc = Jsoup.connect(url)
							.userAgent("Mozilla")
							.timeout(TIME_OUT)
							.get();
		Elements listSong = doc.select("#lyricList > .body > div.title");
		for (Element songElm : listSong) {
			Element songSelector = songElm.select("a[href]").first();
			String songHref = urlMain + songSelector.attr("href");
			String songName = songSelector.text();
			String songIdCrawler = songSelector.attr("href").replaceAll("/artist/([a-zA-Z0-9-_]*)/([a-zA-Z0-9-_]*).html", "$2");
			// System.out.println("---- " + songName + " > " + songHref);
			this.fetchLyric(songHref, songIdCrawler, artistId);
			// break;
		}
		System.out.println("=============== END ===============\n");
	}

	public void fetchLyric(String url, String songIdCrawler, int artistId) throws IOException
	{
		Document doc = Jsoup.connect(url)
							.userAgent("Mozilla")
							.timeout(TIME_OUT)
							.get();
		Element lyricBlock = doc.select("#lyricBlock").first();

		String name = lyricBlock.getElementsByTag("h2").first().text();

		String lyrictist = "";
		Pattern lyrictistParttern = Pattern.compile("作詞：(.*)");
		String composer = "";
		Pattern composerParttern = Pattern.compile("作曲：(.*)");
		Elements infoElm = lyricBlock.getElementsByTag("td");
		for (Element info : infoElm) {
			Matcher matcherLyric = lyrictistParttern.matcher(info.text());
			if (matcherLyric.find()) {
				lyrictist = matcherLyric.group(1);
			}
			Matcher matcherComposer = composerParttern.matcher(info.text());
			if (matcherComposer.find()) {
				composer = matcherComposer.group(1);
			}
		}

		String lyrics = Jsoup.parse(lyricBlock.getElementById("lyricBody").html().replaceAll("(?i)<br[^>]*>", "br2n")).text();
		lyrics = lyrics.replaceAll("br2n", "\n");
		System.out.println("-------- " + name + " > [" + lyrictist + " | " + composer + "]");
		// System.out.println("------------\n" + lyrics + "\n------------");
		Song song = new Song();
		song.setSongIdCrawler(songIdCrawler);
		song.setArtistId(artistId);
		song.setName(name);
		song.setLyrictist(lyrictist);
		song.setComposer(composer);
		song.setLyrics(lyrics);
		songModel.insert(song);
		// System.exit(0);
	}
}
