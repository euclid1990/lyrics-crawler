package crawler.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import crawler.model.DAOManager;

public class Application {

	public static void main(String[] args) throws Exception {
		// Read configurations
		readProperties();
		// Initial database connections
		DAOManager dao = DAOManager.getInstance();
		// Open database connections
		dao.open();
		// Get Lyrics from web
		Crawler webCrawler = new Crawler();
		webCrawler.fetch();
		// Close database connections
		dao.close();
	}

	public static void readProperties()
	{
		String propFileName = "application.properties";
		String configFileName = System.getProperty("config");
		Properties prop = new Properties();
     	FileInputStream file = null;
     	InputStream input = null;
		try {
     		if (configFileName != null) {
     			file = new FileInputStream(configFileName);
     			prop.load(file);
    		} else {
    			input = Application.class.getClassLoader().getResourceAsStream(propFileName);
    			prop.load(input);
    		}
            // Get the property value configuration
			Crawler.START_PAGE = Integer.parseInt(prop.getProperty("start_page"));
			Crawler.END_PAGE = Integer.parseInt(prop.getProperty("end_page"));
			DAOManager.DB_URL = prop.getProperty("db_url");
			DAOManager.DB_USER = prop.getProperty("db_user");
			DAOManager.DB_PASS = prop.getProperty("db_pass");

    	} catch (IOException e) {
    		e.printStackTrace();
        } finally {
        	if (file != null){
        		try {
        			file.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
        	}
        	if (input != null){
        		try {
        			input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
        	}
        }
	}

}
