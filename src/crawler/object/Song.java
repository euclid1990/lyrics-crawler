package crawler.object;

import java.sql.Timestamp;
import java.util.Date;

public class Song {

	private int id;
	private String songIdCrawler;
	private int artistId;
    private String name;
    private String lyrictist;
    private String composer;
    private String lyrics;
    private Timestamp deletedAt;
    private Timestamp createdAt;
    private Timestamp updatedAt;

	public Song() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSongIdCrawler() {
		return songIdCrawler;
	}

	public void setSongIdCrawler(String songIdCrawler) {
		this.songIdCrawler = songIdCrawler;
	}

	public int getArtistId() {
		return artistId;
	}

	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLyrictist() {
		return lyrictist;
	}

	public void setLyrictist(String lyrictist) {
		this.lyrictist = lyrictist;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public String getLyrics() {
		return lyrics;
	}

	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCurrentTimeStamp()
	{
		Date today = new Date();
		return new Timestamp(today.getTime());
	}

}
