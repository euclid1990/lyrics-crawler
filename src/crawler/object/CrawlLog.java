package crawler.object;

import java.sql.Timestamp;
import java.util.Date;

public class CrawlLog {

	private int id;
	private int page;
	private String url;
    private Timestamp deletedAt;
    private Timestamp createdAt;
    private Timestamp updatedAt;

	public CrawlLog() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCurrentTimeStamp()
	{
		Date today = new Date();
		return new Timestamp(today.getTime());
	}

}
