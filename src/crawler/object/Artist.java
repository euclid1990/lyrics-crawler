package crawler.object;

import java.sql.Timestamp;
import java.util.Date;

public class Artist {

	private int id;
	private String artistIdCrawler;
    private String name;
    private Timestamp deletedAt;
    private Timestamp createdAt;
    private Timestamp updatedAt;

	public Artist() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArtistIdCrawler() {
		return artistIdCrawler;
	}

	public void setArtistIdCrawler(String artistIdCrawler) {
		this.artistIdCrawler = artistIdCrawler;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Timestamp deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Timestamp getCurrentTimeStamp()
	{
		Date today = new Date();
		return new Timestamp(today.getTime());
	}
}
